global.BITSO_BASE = "https://api.bitso.com/v3/";
global.COIN_MARKET = "https://api.coinmarketcap.com/v1/";
global.KRAKEN_PUBLIC = "https://api.kraken.com/0/public/";
global.POLONIEX_PUBLIC = "https://poloniex.com/public";
global.SLACK = "https://hooks.slack.com/services/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";
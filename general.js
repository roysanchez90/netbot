var express = require('express');
var request = require('request');
var router = express.Router();

router.get('/', function(req, res) {
    console.log(global.BITSO_BASE);
    res.send('GET route on general.');

});

router.get('/available', function(req, res) {

    request(global.BITSO_BASE + "available_books/",
        function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var json_body = JSON.parse(body);
                console.log(json_body.payload);
            }
        })
    res.send();

});

router.get('/price/:coin', function(req, res) {

    request(global.COIN_MARKET + "ticker/" + req.params.coin,
        function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var json_body = JSON.parse(body);
            }
        })
    res.send();

});

router.post('/', function(req, res) {
    res.send('POST route on things.');
});

//export this router to use in our index.js
module.exports = router;
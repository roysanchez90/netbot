var request = require('request');

module.exports = {
    getActualPrice: function(cryptocoin, callback) {
        request(global.BITSO_BASE + "ticker?book=" + cryptocoin + "/",
            function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    var json_body = JSON.parse(body);
                    callback(null, json_body[0].last);
                }
            });
    },
};
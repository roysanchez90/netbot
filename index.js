var express = require('express');
var general = require('./general.js');
var configFile = require('./config.js');
var slack = require('./slack/index.js');
var coinmarket_public = require('./coinmarket/public_transaction.js');
var app = express();
var cron = require('node-cron');

var cryptocoins = ["ethereum", "bitcoin-cash", "ripple"];
var crypto_entity = {};

app.get('/calculate', function(req, res) {
    calculatePrice();
    res.send("Hello world!");
});


app.get('/cron', function(req, res) {
    cron.schedule('* * * * *', function() {
        console.log('running a task every minute');
        calculatePrice();
    });
});


app.get('/calculate/getinfo', function(req, res) {

    res.send(crypto_entity);
});




function calculatePrice() {
    coinmarket_public.getActualPriceBTC(null,
        function(err, body) {
            if (err) {
                console.log(err);
            } else {
                var message = "";

                for (var i = body.length - 1; i >= 0; i--) {
                    if (cryptocoins.includes(body[i].id)) {

                        var body_id = body[i].id;
                        if (undefined == crypto_entity[body_id]) {
                            crypto_entity[body_id] = { "price": body[i].price_btc };
                        } else {

                            if (crypto_entity[body_id].actual_price == body[i].price_btc) {
                                continue;
                            }

                            crypto_entity[body_id].actual_price = body[i].price_btc;
                            /**

                            Debug only

                            **/

                            var saved_price = crypto_entity[body_id].price;
                            var online_price = body[i].price_btc;
                            var porcentaje = ((online_price - saved_price) *
                                100) / saved_price;
                            console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                            console.log("Moneda ::: " + body[i].name);
                            console.log("saved_price ::: " + saved_price);
                            console.log("online_price ::: " + online_price);
                            console.log("porcentaje ::: " + porcentaje);
                            console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

                            /**

                            END Debug only

                            **/




                            if (Math.abs(porcentaje) > 5) {

                                slack.sendMessage("Putos! El precio del " + body[i].name + " es de Ƀ " +
                                    online_price + " y ha cambiado un " + porcentaje + "% \n" +
                                    " Precio anterior: " + saved_price);
                            }
                        }

                    }
                }

            }
            console.log(message);
            //	slack.sendMessage("Putos! El precio del ether :ether: es de Ƀ "+ body);
        });
}

app.listen(3000);
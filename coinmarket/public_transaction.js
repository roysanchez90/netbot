var request = require('request');

module.exports = {
    getActualPriceBTC: function(cryptocoin, callback) {

        if (cryptocoin == null) {
            request(global.COIN_MARKET + "ticker/",
                function(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var json_body = JSON.parse(body);
                        callback(null, json_body);
                    }
                });
        } else {
            request(global.COIN_MARKET + "ticker/" + cryptocoin + "/",
                function(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var json_body = JSON.parse(body);
                        callback(null, json_body[0].price_btc);
                    }
                });
        }

    },
    getActualPriceUSD: function(cryptocoin, callback) {
        request(global.COIN_MARKET + "ticker/" + cryptocoin + "/",
            function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    var json_body = JSON.parse(body);
                    callback(null, json_body[0].price_usd);
                }
            });
    }
};
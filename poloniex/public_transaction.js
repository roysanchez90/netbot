var request = require('request');

module.exports = {
    getActualPriceBTC: function(cryptocoin, callback) {

        request(global.POLONIEX_PUBLIC + "?command=returnTicker",
            function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    var json_body = JSON.parse(body);
                    callback(null, json_body);
                }
            });
    },
    getChartData: function(cryptocoin, start, end, period, callback) {
        request(global.COIN_MARKET + "?command=returnChartData&currencyPair=" + cryptocoin + "&start=" + start + "&end=" + end + "&period=" + period,
            function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    var json_body = JSON.parse(body);
                    callback(null, json_body);
                }
            });
    }
};